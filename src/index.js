import React from 'react';
import ReactDOM from 'react-dom';
import App from './app_code/AppRoot';

ReactDOM.render(<App/>, document.getElementById('root'));
