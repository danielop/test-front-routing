import React from 'react';
import model from '../../hooks/my_app';
import './styles.css';
import { CircularProgress, Slider } from '@material-ui/core';
import { withRouter } from 'react-router-dom'
import Dashboard from '../dashboard'
import Snakbar from '../../components/snakbar'
import {Loader} from 'google-maps'
import MarkerClusterer from '@google/markerclustererplus'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import _ from 'lodash'

const loader = new Loader('AIzaSyDr6RXud6i5x8dx58y4dXbv6aQUZMnhquo');

function MapView(props) {
  
    const [store, setStore] = React.useState({
      loading: true,
      users: [],
      showModal: false,
      email: '',
      password: '',
      msgError: '',
      username: '',
      messageSnak: '',
      limitUrl: 500,
      markers: [],
      map: null,
      statusSelected: 'ALL'
    });

    const showError = (msg) => {
      setStore(oldValues => ({
        ...oldValues,
        loading: false,
        msgError: msg,
        openSnak: true,
        messageSnak: msg || 'Error de conexión',
        variantSnak: 'error'
      }))
    }

    const closeSnak = () => {
      setStore(oldValues => ({
        ...oldValues,
        openSnak: false,
        messageSnak: '',
        txtBtSnack: '',
        fncBtSnack: () => {}
      }))
    }

    const renderAll = async (points = [], filter = '') => {
      const filterSelected = filter || store.statusSelected
      const locations = points.filter((point) => {
        return filterSelected === 'ALL' ? true : point.status === filterSelected
      }).map((point) => {
        return  {lat: point.origin_latitude, lng: point.origin_longitude}
      })
      
      const google = await loader.load()
      const map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.4541423, lng: -70.776467},
        zoom: 10,
        disableDefaultUI: true,
        zoomControl: true,
      })
      const markers = locations.map((location, i)  =>{
        return new google.maps.Marker({
          position: location,
          label: `${i}`
        });
      });

      new MarkerClusterer(map, markers, {
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
      })

      setStore(oldValues => ({
        ...oldValues,
        loading: false,
        markers: points,
        map: map
      }))
    }

    const getMarkers = () => {
      
      model.getMap({
        total: store.limitUrl
      }).then((res) => {
        if (res.ok && res.data.status === 'success') {
          renderAll(res.data.points)
        } else {
          showError(_.get(res, 'data.errors', ''))
        }
      }).catch((e) => {
        showError(_.get(e, 'data.errors', 'Error de conexión'))
      })
    }

    const changeRange = () => {
      setStore(oldValues => ({
        ...oldValues,
        loading: true
      }))
      getMarkers()
    }

    const handleChange = event => {
      setStore(oldValues => ({
        ...oldValues,
        statusSelected: event.target.value
      }))
      renderAll(store.markers, event.target.value)
    };

    React.useEffect(() => {
      getMarkers()
    }, []);

    return (
      <Dashboard tabActive={2}>
        <div className="container-controls">
          <FormControl className='formControl'>
            <InputLabel id="select-status" shrink>
              Estado de los markers
            </InputLabel>
            <Select
              labelId="select-status"
              value={store.statusSelected}
              onChange={handleChange}
            >
              <MenuItem value={'ALL'}>Todos</MenuItem>
              <MenuItem value={'SUCCESS'}>Creados</MenuItem>
              <MenuItem value={'PENDING'}>Pendientes</MenuItem>
              <MenuItem value={'FAILURE'}>Fallidos</MenuItem>
            </Select>
          </FormControl>
          <p>Cantidad de puntos: </p>
          <Slider
            value={store.limitUrl}
            aria-labelledby="discrete-slider"
            valueLabelDisplay="auto"
            min={0}
            max={10000}
            onChange={(e, value) => {
              setStore(ov => ({...ov, limitUrl: value}))
            }}
            onChangeCommitted={changeRange}
          />
        </div>
        <div className="map-container">
          <div id="map" className="map"></div>
        </div>
        {store.loading && <div className='contLoading'>
          <CircularProgress />
        </div>}
        <Snakbar
          message={store.messageSnak}
          open={store.openSnak}
          handleClose={closeSnak}
          variant={store.variantSnak}
          textBt={store.txtBtSnack}
          onClick={store.fncBtSnack}
        />
      </Dashboard>
    );
}

export default withRouter(MapView);

