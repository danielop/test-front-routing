import React from 'react';
import model from '../../hooks/my_app';
import './styles.css';
import { TextField, Button, CircularProgress } from '@material-ui/core';
import { withRouter } from 'react-router-dom'
import _ from 'lodash'

function Login(props) {
    const [store, setStore] = React.useState({
      email: '',
      password: '',
      msgError: '',
      username: '',
      loading: true,
      typeForm: 'login'
    });

    const changeState = name => event => {
      event.persist()
      setStore(oldValues => ({
        ...oldValues,
        [name]: event.target.value,
        msgError: ''
      }))
    }

    const showError = (msg) => {
      setStore(oldValues => ({
        ...oldValues,
        loading: false,
        msgError: msg
      }))
    }

    const sendForm = () => {
      if (!store.email || !store.password || !store.username) {
        showError('No deje campos vacíos')
      } else if (!validateEmail(store.email)) {
        showError('Ingrese un correo válido')
      } else {
        setStore(oldValues => ({
          ...oldValues,
          loading: true
        }))
        if (store.typeForm === 'login') {
          model.actionLogin({
            user: {
              email: store.email,
              password: store.password,
              username: store.username
            }
          }).then((res) => {
            if (res.ok && res.data.status === 'success' && res.data.logged_in) {
              props.history.push('/users')
            } else {
              showError(_.get(res, 'data.errors', ''))
            }
          }).catch((e) => {
            showError(_.get(e, 'data.errors', 'Error de conexión'))
          })
        } else {
          model.actionSignup({
            user: {
              email: store.email,
              password: store.password,
              username: store.username
            }
          }).then((res) => {
            if (res.ok && res.data.status === 'success') {
              props.history.push('/users')
            } else {
              showError(_.get(res, 'data.errors', ''))
            }
          }).catch((e) => {
            showError(_.get(e, 'data.errors', 'Error de conexión'))
          })
        }
      }
    }

    const pressKeyInput = (e) => {
      if (e.key.toLowerCase() === 'enter') {
        sendForm()
      }
    }

    const validateEmail = (email) => {
      // eslint-disable-next-line
      const exp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
      return !!exp.test(email)
    }

    const changeForm = () => {
      setStore(oldValues => ({
        ...oldValues,
        typeForm: oldValues.typeForm === 'login' ? 'signup' : 'login',
        msgError: ''
      }))
    }

    const hasSession = () => {
      model.actionGetSession().then((res) => {
        if (res.ok && res.data.status === 'success' && res.data.logged_in) {
          props.history.push('/users')
        } else {
          showError(_.get(res, 'data.errors', ''))
        }
      }).catch((e) => {
        showError(_.get(e, 'data.errors', 'Error de conexión'))
      })
    }

    React.useEffect(() => {
      hasSession()
    }, []);

    return (
        <div className="login-container">
          <div className="form-login">
            <h5>
              Bienvenido a la plataforma Test Front Routing, por favor ingrese sus datos para {store.typeForm === 'login' ? 'iniciar sesión' : 'registrarse' }.
            </h5>
            <TextField
              className="input"
              label="Nombre de usuario"
              value={store.username}
              onChange={changeState('username')}
              onKeyPress={pressKeyInput}
            />
            <TextField
              className="input"
              label="Correo electrónico"
              value={store.email}
              onChange={changeState('email')}
              onKeyPress={pressKeyInput}
            />
            <TextField
              className="input"
              label="Contraseña"
              type="password"
              value={store.password}
              onChange={changeState('password')}
              onKeyPress={pressKeyInput}
            />
            <p className="msg-error">
              {store.msgError}
            </p>
            <Button
              variant="contained"
              color="primary"
              className="button-send"
              onClick={sendForm}
            >
              {store.typeForm === 'login' ? 'Iniciar sesión' : 'Registrarse'}
            </Button>
            <Button
              className="button-send"
              onClick={changeForm}
            >
              Ir a {store.typeForm !== 'login' ? 'Iniciar sesión' : 'Registrarse'}
            </Button>
          </div>
          {store.loading && <div className='contLoading'>
            <CircularProgress />
          </div>}
        </div>
    );
}

export default withRouter(Login);

