import React from 'react';
import model from '../../hooks/my_app';
import './styles.css';
import { Button, CircularProgress } from '@material-ui/core';
import { withRouter } from 'react-router-dom'
import { Store } from '../../hooks/main_store'

function Dashboard(props) {
    const {state, dispatch} = React.useContext(Store)
    const [store, setStore] = React.useState({
      loading: true,
    });

    const hasSession = () => {
      model.actionGetSession().then((res) => {
        if (res.ok && res.data.status === 'success' && res.data.logged_in) {
          dispatch({
            type: 'SET_DATA',
            payload: res.data.user,
          })
          setStore((ov) => ({
            ...ov,
            loading: false
          }))
        } else {
          props.history.push('/')
        }
      }).catch(() => {
        props.history.push('/')
      })
    }

    const signoff = () => {
      setStore((ov) => ({
        ...ov,
        loading: true
      }))
      model.signoff().then((res) => {
        if (res.ok && res.data.status === 'success') {
          props.history.push('/')
        } else {
          // TODO error
          setStore((ov) => ({
            ...ov,
            loading: false
          }))
        }
      }).catch(() => {
        // TODO error
        setStore((ov) => ({
          ...ov,
          loading: false
        }))
      })
    }

    React.useEffect(() => {
      hasSession()
    }, []);

    return (
      <div className="dashboard-container">
        <div className="info-user">
          <b>(Usuario Activo) </b>ID: {state.user.id || '-'} / Username: {state.user.username || '-'} / Email: {state.user.email || '-'}
        </div>
        <div className="header">
          <Button
            className={props.tabActive === 1 ? 'bt-active' : 'bt-tab'}
            onClick={() => props.history.push('/users')}
          >
            Usuarios
          </Button>
          <Button
            className={props.tabActive === 2 ? 'bt-active' : 'bt-tab'}
            onClick={() => props.history.push('/map')}
          >
            Mapa
          </Button>
          <Button
            className="bt-signoff"
            onClick={signoff}
          >
            Cerrar sesión
          </Button>
        </div>
        <div className="content-view">
          {props.children}
        </div>
        {store.loading && <div className='contLoading'>
          <CircularProgress />
        </div>}
      </div>
    );
}

export default withRouter(Dashboard);

