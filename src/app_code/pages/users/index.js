import React from 'react';
import model from '../../hooks/my_app';
import './styles.css';
import { TextField, Button, CircularProgress, Modal, Fab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { withRouter } from 'react-router-dom'
import Dashboard from '../dashboard'
import moment from 'moment'
import Snakbar from '../../components/snakbar'
import _ from 'lodash'

function Users(props) {
    const [store, setStore] = React.useState({
      loading: true,
      users: [],
      showModal: false,
      email: '',
      password: '',
      msgError: '',
      username: '',
      messageSnak: ''
    });

    const changeState = name => event => {
      event.persist()
      setStore(oldValues => ({
        ...oldValues,
        [name]: event.target.value,
        msgError: ''
      }))
    }

    const getUsers = () => {
      model.getUsers().then((res) => {
        if (res.data.status === 'success') {
          setStore((ov) => ({
            ...ov,
            loading: false,
            users: res.data.users,
            msgError: ''
          }))
        } else {
          setStore((ov) => ({
            ...ov,
            loading: false,
            msgError: ''
          }))
        }
      }).catch(() => {
        setStore((ov) => ({
          ...ov,
          loading: false,
          msgError: ''
        }))
      })
    }

    const showError = (msg) => {
      setStore(oldValues => ({
        ...oldValues,
        loading: false,
        msgError: msg,
        openSnak: true,
        messageSnak: msg || 'Error de conexión',
        variantSnak: 'error'
      }))
    }

    const renderUsers = () => {
      return store.users.map((user) => <div className="user-item" key={user.id}>
        <p><b>ID: </b>{user.id}</p>
        <p><b>Email: </b>{user.email}</p>
        <p><b>Username: </b>{user.username}</p>
        <p><b>Ingreso: </b>{moment(user.created_at).format('DD/MMM/YYYY, hh:mm a')}</p>
      </div>)
    }

    const toogleModal = () => {
      setStore((ov) => ({
        ...ov,
        showModal: !ov.showModal,
        username: '',
        email: '',
        password: ''
      }))
    }

    const validateEmail = (email) => {
      // eslint-disable-next-line
      const exp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
      return !!exp.test(email)
    }

    const sendForm = () => {
      if (!store.email || !store.password || !store.username) {
        showError('No deje campos vacíos')
      } else if (!validateEmail(store.email)) {
        showError('Ingrese un correo válido')
      } else {
        setStore(oldValues => ({
          ...oldValues,
          loading: true,
          showModal: false
        }))
        model.createUser({
          user: {
            email: store.email,
            password: store.password,
            username: store.username
          }
        }).then((res) => {
          if (res.ok && res.data.status === 'success') {
            getUsers()
          } else {
            showError(_.get(res, 'data.errors', ''))
          }
        }).catch((e) => {
          showError(_.get(e, 'data.errors', 'Error de conexión'))
        })
      }
    }

    const closeSnak = () => {
      setStore(oldValues => ({
        ...oldValues,
        openSnak: false,
        messageSnak: '',
        txtBtSnack: '',
        fncBtSnack: () => {}
      }))
    }

    React.useEffect(() => {
      getUsers()
    }, []);

    return (
      <Dashboard tabActive={1}>
        <div className="users-container">
          {renderUsers()}
        </div>
        {store.loading && <div className='contLoading'>
          <CircularProgress />
        </div>}
        <Modal
          open={store.showModal}
          onClose={toogleModal}
        >
          <div className="form-user">
            <p>Ingrese la siguiente información para crear un nuevo usuario:</p>
            <TextField
              className="input"
              label="Nombre de usuario"
              value={store.username}
              onChange={changeState('username')}
            />
            <TextField
              className="input"
              label="Correo electrónico"
              value={store.email}
              onChange={changeState('email')}
            />
            <TextField
              className="input"
              label="Contraseña"
              type="password"
              value={store.password}
              onChange={changeState('password')}
            />
            <p className="msg-error">
              {store.msgError}
            </p>
            <Button
              variant="contained"
              color="primary"
              className="button-send"
              onClick={sendForm}
            >
              Crear
            </Button>
          </div>
        </Modal>
        <Fab className="bt-fab" onClick={toogleModal}>
          <AddIcon />
        </Fab>
        <Snakbar
          message={store.messageSnak}
          open={store.openSnak}
          handleClose={closeSnak}
          variant={store.variantSnak}
          textBt={store.txtBtSnack}
          onClick={store.fncBtSnack}
        />
      </Dashboard>
    );
}

export default withRouter(Users);

