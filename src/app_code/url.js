// ===================
// Urls file
// ===================

import React from 'react'

import {BrowserRouter, Route, Switch} from "react-router-dom";
import Login from './pages/auth';
import Users from './pages/users';
import Map from './pages/map';



// ===================
// Import pages 
// ===================


const Page404 = ({location}) => (
    <div>
        <h2>No match found for <code>{location.pathname}</code></h2>
    </div>
);


// ===================
// Create router
// ===================

const AppRouter = () => (
    <BrowserRouter>
        <Switch>
            <Route path="/" exact component={Login}/>
            <Route path="/users" exact component={Users}/>
            <Route path="/map" exact component={Map}/>
            <Route component={Page404}/>
        </Switch>
    </BrowserRouter>
);

export default AppRouter;