import React from 'react';

export const Store = React.createContext();

const initialState = {
    user: {}
};

function reducer(state, action) {
    switch (action.type) {
        case 'SET_DATA':
            return {...state, user: action.payload};
        case 'REMOVE_DATA':
            return {
                ...initialState
            };
        default:
            return state;
    }
}

export function StoreProvider(props) {
    const [state, dispatch] = React.useReducer(reducer, initialState);
    const value = {state, dispatch};
    return <Store.Provider value={value}>{props.children}</Store.Provider>;
}
