import apisauce from 'apisauce';

const api = apisauce.create({
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    },
    timeout: 20000,
    baseURL:'https://btfx.herokuapp.com',
    withCredentials: true
});

class my_class_app {

    /*
    * Set / get dispatch needs init in app root 
    */
    dispatch = null;
    get_dispatch = () => {return this.dispatch==null?false:this.dispatch;}
    set_dispatch = (n_dispatch) => {this.dispatch=n_dispatch;}    
    /* End dispatch set */

    // Authentication

    actionLogin = (body) => {
        return api.post('/login', body)
    }

    actionSignup = (body) => {
        return api.post('/signup', body)
    }

    actionGetSession = () => {
        return api.get('/logged_in')
    }

    signoff = () => {
        return api.delete('/logout')
    }

    // UserInfo

    getUsers = () => {
        return api.get('/users')
    }

    createUser = (body) => {
        return api.post('/users', body)
    }

    // MapInfo

    getMap = (body) => {
        return api.get(`/map?limit=${body.total}`)
    }

}

export default new my_class_app();
