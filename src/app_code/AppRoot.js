import React, {Component} from 'react';
import UrlsComponent from './url';
import {Store, StoreProvider} from './hooks/main_store';
import '../static_files/css/app.css';

import model from "./hooks/my_app";


// Function to init dispatch model classes
function InitHooksClasses() {
    const {state, dispatch} = React.useContext(Store);
    // Init dispatch for model or models
    React.useEffect(() => {
        if(!model.get_dispatch()){model.set_dispatch(dispatch)}
    }, [state]);
    return (<React.Fragment />);
}


class AppRoot extends Component {

    render() {
        return (
            <StoreProvider>
                <InitHooksClasses />
                <UrlsComponent />
            </StoreProvider>
        )
    }
}

export default AppRoot;
