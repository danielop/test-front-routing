# Test Front Routing

Este proyecto esta realizado bajo ReactJS v16.8.4.

El objetivo principal es dejar que los usuarios puedan registrarse e iniciar sesión por medio de un formulario principal.

Luego tiene la opción de ver los usuarios registrados en una lista y puede agregar nuevos usuarios por medio de un formulario en un Modal.

En otra vista puede ver un mapa con diferentes puntos generados aleatoriamente, en donde puede filtrarlos por estado y puede cambiar la cantidad de puntos generados.

Por último, el usuario puede cerrar sesión y borrar los datos almacenados.

## 🚀 Ejecutar aplicación

1. Los requisitos para poder ejecutar este proyecto los puede verificar en [esta página de ReactJS](https://es.reactjs.org/docs/getting-started.html)

2. NOTA: La aplicación está estructurada por medio de `Pages`, `Components` y `Hooks`.

3.  Al descargar el proyecto se debe instalar las correspondientes dependencias

    ```sh
    cd test-front-routing/
    yarn install
    ```

4.  Para inicar el proyecto y verlo en modo live

    ```sh
    yarn start
    ```


